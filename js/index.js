// 1. Описати своїми словами навіщо потрібні функції у програмуванні.

// Функція  це як інша частина в проекі, яку ми можемо записати окремо від головної програми, можемо не думати на рахунок інших частин коду. Ми можемо розбирпти складні завдання на легші і робити відокремлено 

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.

// Тому що таку функцію потім можна викликати в будь який момент

// 3. Що таке оператор return та як він працює всередині функції?

// return це оператор який повертає значення з функції. За допомогою нього ми повертаємо результат виконання функції, якщо його не буде то функція поверне undefind

const validateNumber = number => !(number === null || number === "" || isNaN(number)); // !number

const getNumber = (message = "Enter your number") => {

    let userNumber;

    do {
        userNumber = prompt(message);
    } while (!validateNumber(userNumber))

    return +userNumber;

}


const calcSumNumber = (a, b) => a+b;
const calcsubtractionNumber = (a, b) => a-b;
const calcProductNumber = (a, b) => a*b;
const calcDivisionNumber = (a, b) => a/b;



const calcNumber = (method) => {


    switch (method) {
        case `+`: { 
            const a = +getNumber("Введіть перше число");
            const b = +getNumber("Введіть друге число");
            return calcSumNumber(a, b);
        };

        case `-`: { 
            const a = +getNumber("Введіть перше число");
            const b = +getNumber("Введіть друге число");
            return calcsubtractionNumber(a, b);
        };

        case `*`: { 
            const a = +getNumber("Введіть перше число");
            const b = +getNumber("Введіть друге число");
            return calcProductNumber(a, b);
        };

        case `/`: { 
            const a = +getNumber("Введіть перше число");
            const b = +getNumber("Введіть друге число");
            return calcDivisionNumber(a, b);
        };
        
    }
}

let calcType;

do {
    calcType = prompt('Введіть  тип  математичного розвязку: `+`, `-`, `*`, `/`');
} while (calcType !== '+' && calcType !== '-' && calcType !== '*' && calcType !== '/')




console.log(calcNumber(calcType));